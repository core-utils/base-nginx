# Инструкция для создания образа с определенным набором модулей

> Инструкция https://github.com/nginxinc/docker-nginx/tree/6f0396c1e06837672698bc97865ffcea9dc841d5/modules

Выполнить для создани нужного образа `docker build --build-arg ENABLED_MODULES="ndk lua" -t my-nginx .`

* my-nginx - название вашего образа

---

## Текущая сборка базовая сборка


docker build --build-arg ENABLED_MODULES="xslt geoip image-filter perl njs brotli" -t registry.gitlab.com/core-utils/base-nginx -f Dockerfile.alpine .


---

## Пушим в репозиторий

echo "$ACCESS_TOKEN" | docker login registry.gitlab.com --username Deployer --password-stdin

docker push registry.gitlab.com/core-utils/base-nginx
